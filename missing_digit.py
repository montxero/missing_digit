#!/usr/bin/env python
# coding: utf-8

# Author: Seyi MontXero

# # Missing Digit
#
# Have the function `missing_digit(st)` take the `st` parameter,
# which will be a simple mathemaical formula with three numbers,
# a single operator`(+, -, * or / )` and an equal sign `(=)` and return
# the digit that completes the equation.
# In one of the numbers of th equation, there will be an `x`
# character and your program should determine what digit is missing.
#
# For example, if `str` is `"3x + 12 = 46"`
# then your program should output 4.
# The `x` character can appear in any of the three numbers and all three
# numbers will be  greater than or equal to 0, and less than or equal to
# 1000000.
#
# ## Examples
#     Input: "4 - 2 = x"
#     Output: 2
#
#     Input: "1x0 * 12 = 1200"
#     Output: 0

# _Strategy_
#
# 1. Parse the input string to get an equation
# 2. normalise the equation: collect the known numbers to a single expression
# 3. evaluate the expression with known numbers
# 4. convert the result of step 3 to a string
# 5. compare the result of step 4 character-wise with the number with
#    the unknown digit to get the unknown digit


from typing import Tuple, Union

# Type declarations: not necessary but makes things clearer
ThreeTupleExpression = Tuple[str, str, str]
SingletonExpression = Tuple[str]
Expression = Union[ThreeTupleExpression, SingletonExpression]
Equation = Tuple[ThreeTupleExpression, SingletonExpression]


def parse_equation(eqn: str) -> Equation:
    """return the equation object resulting from parsing `eqn`.
    The input equation `eqn` is of the form:
                "a op b = c"
    where a, b, and c are numbers, and op is one of + - * /.
    The resulting equation object has the folloing form
               ((op, a, b), (c,))
    From the structure of the return type, we see that the
    first term is parsed into prefix notation.

    Parameters
    ----------
    eqn: str, the string representation of the input equation

    Returns
    -------
    Equation

    Examples
    ---------
        >>> parse_equation("4 - 2 = x")
        (('-', '4', '2'), ('x',))

        >>> parse_equation("1x0 * 12 = 1200")
        (('*', '1x0', '12'), ('1200',))
    """
    a, op, b, _, c = eqn.split()
    return ((op, a, b), (c,))


def normalise(eqn: Equation) -> Equation:
    """return an equation equivalent to `eqn` where the known quantities are
    grouped together and the number with the unknown digit is on its own.
    This effectively makes the number with the unknown digit the subject of
    the formula.

    Parameters
    ----------
    eqn: Equation, the input equation

    Returns
    -------
    Equation

    Examples
    --------
        >>> normalise((('-', '4', '2'), ('x',)))
        (('-', '4', '2'), ('x',))

        >>> normalise((('*', '1x0', '12'), ('1200',)))
        (('/', '1200', '12'), ('1x0',))

        >>> normalise((('+', '15', '2x'), ('37')))
        (('-', '37', '15'), ('2x',))

        >>> normalise((('-', '32', 'x7), ('5',)))
        (('-', '32', '5'), ('x7',))
    """
    dct = {'+': '-',
           '*': '/',
           '-': '+',
           '/': '*'}
    lhs, rhs = eqn
    c = rhs[0]  # get the string from the tuple
    if 'x' in c:  # simplest case, eqn is already normalised
        return eqn
    op, a, b = lhs
    if 'x' in a:  # next easiest case, just move b over to the other side
        new_lhs = (dct[op], c, b)
        new_rhs = (a,)
        return (new_lhs, new_rhs)
    # last possible case: 'x' is in b
    if op in ('+', '*'):  # in this case, we must change the operator
        new_op = dct[op]
        new_lhs = (new_op, c, a)
        new_rhs = (b,)
        return (new_lhs, new_rhs)
    else:
        new_op = op  # the operator does not change if it is a - or /
        new_lhs = (new_op, a, c)
        new_rhs = (b,)
    return (new_lhs, new_rhs)


def evaluate_expression(expr: Expression) -> Expression:
    """return a new expression which holds the value of evaluating expr

    Parameters
    ----------
    expr: Expression, the expression to be evaluated

    Returns
    -------
    Expression

    Examples
    --------
        >>> evaluate_expression(('-', '37', '15'))
        ('22',)

        >>> evaluate_expression(('10'),)
        ('10',)
    """
    if len(expr) == 1:
        return expr
    op = expr[0]
    a, b = map(int, expr[1:])
    if op == '+':
        return (str(a+b),)
    if op == '-':
        return (str(a-b),)
    if op == '*':
        return (str(a*b),)
    if op == '/':
        return (str(a//b),)
    raise ValueError("Invalid operator")

def get_missing_digit(eqn: str) -> str:
    """return the missing digit according to the problem specification"""
    lhs, rhs = normalise(parse_equation(eqn))
    full_num = evaluate_expression(lhs)[0]
    num_with_unknown = rhs[0]
    for i, j in zip(num_with_unknown, full_num):
        if i == 'x':
            return j
    raise(ValueError("Unknown digit not found"))


if __name__ == "__main__":
    for eq in ["4 - 2 = x", "1x0 * 12 = 1200"]:
        print(f"{eq}, => x={get_missing_digit(eq)}")
